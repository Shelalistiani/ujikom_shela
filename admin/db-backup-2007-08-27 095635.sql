

CREATE TABLE `admin` (
  `id_admin` char(16) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama_admin` varchar(20) NOT NULL,
  `id_level` int(2) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO admin VALUES("1","admin123","0192023a7bbd73250516f069df18b500","shela Listiani","1");





CREATE TABLE `level` (
  `id_level` char(20) NOT NULL,
  `nama_level` varchar(20) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");





CREATE TABLE `pelanggan` (
  `id_pelanggan` varchar(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nomor_kwh` varchar(20) NOT NULL,
  `nama_pelanggan` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `saldo` int(20) NOT NULL,
  `id_tarif` int(16) NOT NULL,
  PRIMARY KEY (`id_pelanggan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pelanggan VALUES("20190311001","algifari","69e540022952c60ad26e9f38f31c68f9","091011918","Muhammad Asyraf algifari","Sawah baru","0","1"),
("20190311010","lala","9ccc598773c3b627675dcf47d1b16c5b","01991010","Lala listiani","Bogor","0","2"),
("20190311011","shelalistiani","261363aec0323bd187db709a6dc5fff4","17171911","Shela Listiani","Pagelaran","0","1"),
("20190316001","sila124","e10adc3949ba59abbe56e057f20f883e","1222","sila","Sawah Baru II","0","1"),
("20190321001","aurora123","2bc1644cb1d445444322f5829c2ad70f","91019919111","Aurora Safira","Sawah Baru","0","2"),
("20190326001","lia123","eae61f0edaeab4bc53da35d3458acd67","797887655","lia ananda sari","Pagelaran","0","1");





CREATE TABLE `pembayaran` (
  `id_pembayaran` varchar(12) NOT NULL,
  `id_tagihan` varchar(16) NOT NULL,
  `id_pelanggan` varchar(16) NOT NULL,
  `tanggal_pembayaran` datetime NOT NULL,
  `bulan_bayar` varchar(2) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `biaya_admin` int(20) NOT NULL,
  `total_bayar` int(20) NOT NULL,
  `id_admin` char(16) NOT NULL,
  PRIMARY KEY (`id_pembayaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pembayaran VALUES("20190321001","20190321001","20190311011","2019-03-21 15:17:51","3","135000","2500","137500","Lala"),
("20190321003","20190321003","20190321001","2019-03-21 15:59:21","3","1000000","2500","1002500","shela"),
("20190329001","20190329001","20190311001","2019-03-29 09:43:49","3","135000","2500","137500","shela"),
("20190329002","20190329004","20190326001","2019-03-29 13:57:35","3","135000","2500","137500","shela");





CREATE TABLE `saldo` (
  `id_saldo` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `jumlah_isi` int(10) NOT NULL,
  `metode` varchar(30) NOT NULL,
  `tanggal_pengisian` datetime NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`id_saldo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO saldo VALUES("20070827001","shelalistiani","100000","Bank BCA","2007-08-28 01:11:58","Menunggu Verifikasi"),
("20190329001","algifari","100000","Bank BRI","0000-00-00 00:00:00","Menunggu Verifikasi");





CREATE TABLE `tagihan` (
  `id_tagihan` varchar(12) NOT NULL,
  `id_penggunaan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `bulan` int(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `jumlah_meter` int(20) NOT NULL,
  `status` varchar(15) NOT NULL,
  PRIMARY KEY (`id_tagihan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO tagihan VALUES("20190321002","20190321002","20190321002","3","2019","100","Lunas"),
("20190321003","20190321003","20190321001","3","2019","20","Lunas"),
("20190323001","20190321001","20190311011","3","2019","100","Lunas"),
("20190329001","20190329001","20190311001","3","2019","100","Lunas"),
("20190329002","20190329002","20190326001","3","2019","100","Lunas");





CREATE TABLE `tarif` (
  `id_tarif` int(2) NOT NULL AUTO_INCREMENT,
  `daya` varchar(5) NOT NULL,
  `tarifperkwh` int(5) NOT NULL,
  PRIMARY KEY (`id_tarif`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO tarif VALUES("1","900","1350"),
("2","1600","2500"),
("8","600","1000");



