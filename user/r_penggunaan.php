<?php
include "koneksi.php";
session_start();
if(!isset($_SESSION['id_pelanggan']))
{ 
  header("location:../login/index.php");
}
else{
  $query_pelanggan=mysqli_query($konek,"SELECT * FROM pelanggan where id_pelanggan='$_SESSION[id_pelanggan]'");
  $pelanggan=mysqli_fetch_array($query_pelanggan);
  
} 
?>

<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>PPOB PAYSEL</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="../assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   <!-- TABLE STYLES-->
    <link href="../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />


</head>
<body>

    <div id="wrapper">
       <?php
       include"navbar.php";
       ?>
<div id="page-wrapper" >
	
            <div id="page-inner">
			
              

				<br/>
       
							<div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <h5><b>Riwayat Penggunaan</b></h5>
                        
                        </div>
 
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
					  
 	<th>No penggunaan</th>
								<th>Id Pelangggan</th>
							  <th>Bulan</th>                  
							  <th>Tahun</th>
							  <th>Meter Awal</th>
							  <th>Meter Akhir</th>
                <th>Jumlah Penggunaan</th>
                 
	</tr> </thead>
	<tbody>
        <?php
include"koneksi.php";

	 $query_penggunaan=mysqli_query($konek,"SELECT * FROM penggunaan where id_pelanggan='$_SESSION[id_pelanggan]'");
  while($penggunaan=mysqli_fetch_array($query_penggunaan))
  {

    ?>


<tr><td><?php echo $penggunaan['id_penggunaan'];?></td>
	<td><?php echo $penggunaan['id_pelanggan'];?></td>
	<td><?php echo $penggunaan['bulan'];?></td>
	<td><?php echo $penggunaan['tahun'];?></td>
	<td><?php echo $penggunaan['meter_awal'];?></td>
	<td><?php echo $penggunaan['meter_akhir'];?></td>
  <td><?php $jumlah_penggunaan = $penggunaan['meter_akhir'] - $penggunaan['meter_awal']; echo $jumlah_penggunaan; ?></td>
	
	
		<?php
  }
	?>
	
	
	
	</tr>
               
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
</div>
        </div>
               
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
	 
	 <?php
   include"footer.php";
   ?>
	
	
</body>
</html>
