<?php
include "koneksi.php";
session_start();
if(!isset($_SESSION['id_pelanggan']))
{ 
  header("location:../login/index.php");
}
else{
  $query_pelanggan=mysqli_query($konek,"SELECT * FROM pelanggan where id_pelanggan='$_SESSION[id_pelanggan]'");
  $pelanggan=mysqli_fetch_array($query_pelanggan);
}

	?>

<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>PPOB PAYSEL</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="../assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   <!-- TABLE STYLES-->
    <link href="../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />


</head>
<body>

    <div id="wrapper">
        <?php
        include"navbar.php";
        ?>
<div id="page-wrapper" >
	
            <div id="page-inner">
			
              

				<br/>
       
							<div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <h5><b>Riwayat Pembayaran</b></h5>
                        
                        </div>
 
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
					  
 	<th>No Pembayaran</th>
							  <th>Tanggal Pembayaran</th>                
							  <th>Total Bayar</th>
							  <th>Aksi</th>
                 
	</tr> </thead>
	<tbody>
        <?php
include"koneksi.php";
	 $query_pembayaran=mysqli_query($konek,"SELECT * FROM pembayaran WHERE id_pelanggan='$_SESSION[id_pelanggan]'");
 while($pembayaran=mysqli_fetch_array($query_pembayaran))
  { 
 ?>

<tr><td><?php echo $pembayaran['id_pembayaran'];?></td>
                 <td><?php echo $pembayaran['tanggal_pembayaran'];?></td>
                 <td><?php echo $pembayaran['total_bayar'];?></td>
				 <td><button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                            Detail
                            </button></td>
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 style="color:black;" class="modal-title" id="myModalLabel">Detail Pembayaran</h4>
                                        </div>
                                        <div class="modal-body">
                                            
											<div class="row">
								<div class="col-md-4">
								<div class="form-group">
								<label>No Pembayaran</label>
								</div>
								</div>
								<div class="col-md-6">
								<div class="form-group">
								<input type="number" name="#" class="form-control" VALUE="<?php echo $pembayaran['id_pembayaran'];?>" readonly>
								</div></div></div>
								
								<div class="row">
								<div class="col-md-4">
								<div class="form-group">
								<label>Nama Pelanggan</label>
								</div>
								</div>
								<div class="col-md-6">
								<div class="form-group">

								<input type="text" name="#" class="form-control" VALUE="<?php echo $pelanggan['nama_pelanggan'];?>" disabled>
								</div></div></div>
											
											<div class="row">
								<div class="col-md-4">
								<div class="form-group">
								<label>Nomor KWH</label>
								</div>
								</div>
								<div class="col-md-6">
								<div class="form-group">
								<input type="number" name="#" class="form-control" VALUE="<?php echo $pelanggan['nomor_kwh'];?>" readonly>
								</div></div></div>
											<div class="row">
								<div class="col-md-4">
								<div class="form-group">
								<label>Tanggal Pembayaran</label>
								</div>
								</div>
								<div class="col-md-6">
								<div class="form-group">
								<input type="text" name="#" class="form-control" VALUE="<?php echo $pembayaran['tanggal_pembayaran'];?>" readonly>
								</div></div></div>
											
								<div class="row">
								<div class="col-md-4">
								<div class="form-group">
								<label>Bulan Bayar</label>
								</div>
								</div>
								<div class="col-md-6">
								<div class="form-group">
								<input type="number" name="#" class="form-control" VALUE="<?php echo $pembayaran['bulan_bayar'];?>" readonly>
								</div></div></div>
											<div class="row">
								<div class="col-md-4">
								<div class="form-group">
								<label>Jumlah Bayar</label>
								</div>
								</div>
								<div class="col-md-6">
								<div class="form-group">
								<input type="number" name="#" class="form-control" VALUE="<?php echo $pembayaran['jumlah_bayar'];?>" readonly>
								</div></div></div>
											<div class="row">
								<div class="col-md-4">
								<div class="form-group">
								<label>Biaya Admin</label>
								</div>
								</div>
								<div class="col-md-6">
								<div class="form-group">
								<input type="number" name="#" class="form-control" VALUE="<?php echo $pembayaran['biaya_admin'];?>" readonly>
								</div></div></div>
								<div class="row">
								<div class="col-md-4">
								<div class="form-group">
								<label>Total Pembayaran</label>
								</div>
								</div>
								<div class="col-md-6">
								<div class="form-group">
								<input type="number" name="#" class="form-control" VALUE="<?php echo $pembayaran['total_bayar'];?>" readonly>
								</div></div></div>
											
											<div class="row">
								<div class="col-md-4">
								<div class="form-group">
								<label>Nama Admin</label>
								</div>
								</div>
								<div class="col-md-6">
								<div class="form-group">
								<input type="text" name="#" class="form-control" VALUE="<?php echo $pembayaran['id_admin'];?>" readonly	>
								</div></div></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- End Modals-->
                
	
		
	
	
	
	<?php
}
?>
	</tr>
               
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
</div>
        </div>
               
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
	 <?php
	 include"footer.php";
	 ?>

</body>
</html>
