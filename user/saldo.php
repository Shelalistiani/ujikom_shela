<?php
include"header.php"
?>

<body>
    <div id="wrapper">
        <?php
        include"navbar.php";
        ?>
 <div id="page-wrapper" >
            <div id="page-inner">
		<div class="row">
          <div class="col-md-12">  
           <h5 style="font-size:23px;" class="pull-left">Jumlah Saldo Anda: Rp. <?php echo $pelanggan['saldo']; ?></h5>
           
          </div>
        </div>
       <!-- /. ROW  -->        
       <hr style="background-color:cyan;"/> 
    
      <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                          <h5 style="color:black;"><b>Form Isi Saldo</b></h5>
                        </div>
                        <div class="panel-body">
                           
						   
						   <br/>
						   <form action="proses_saldo.php" method="POST">
				<div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                        <label>Username</label>
                      </div>
                        </div>
                        
                        <div class="col-md-6 col-sm-12">
                        <div class="form-group ">
                        <input class="form-control" type="text" name="Username" placeholder="Username" value="<?php echo $_SESSION['username'];?>" readonly>
                      </div>
                        </div>
                        </div>
						
						
						<div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                        <label>Nominal Pembelian</label>
                      </div>
                        </div>
                        
                        <div class="col-md-6 col-sm-12">
                        <div class="form-group ">
                        <input class="form-control" type="number" name="jumlah_isi" placeholder="Nominal Pembelian" required></input>
                      </div>
                        </div>
                        </div>
                           <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                        <label>Metode Pembayaran</label>
                      </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                        <select class="form-control" name="metode">
                          <option selected="#">Silahkan Anda Memilih Bank</option>
                          <option value="Bank BRI">Bank BRI</option>
                          <option value="Bank BNI">Bank BNI</option>
                          <option value="Bank BCA">Bank BCA </option>
                          <option value="Alfamart">Alfamart </option>
                           <option value="Indomaret">Indomaret </option>

                          </select>
                      </div>
                        </div>


                        <br/>
                        <button class="btn btn-primary pull-right " style="min-width:100px;" type="submit" name="simpan">Beli</button>
                        </div></div>
              
                    </div>
                  </div>
                </div>
                 <!-- /. ROW  -->
					
<br/>
				 
    </div>
             <!-- /. PAGE INNER  -->
			 
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
		</div>
     <!-- /. WRAPPER  -->
   
   <?php
   include"footer.php";
   ?>