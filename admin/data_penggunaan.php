<?php
include"header.php";
?>
			<body>
    <div id="wrapper">
<?php 
include"navbar.php";
?>
<div id="page-wrapper" >
		
            <div id="page-inner">
		
                    <br/>
			<br/>
		
		
			<div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <h5><b>Data Penggunaan Listrik</b></h5>
                        
                        </div>
 
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
					  
 	<th>No penggunaan</th>
								<th>Nama Pelangggan</th>
							  <th>Bulan</th>                  
							  <th>Tahun</th>
							  <th>Meter Awal</th>
							  <th>Meter Akhir</th>
							  <th>Aksi</th>
                 
	</tr> </thead>
	<tbody>
              
	<?php
	$querypenggunaan=mysqli_query($konek,"SELECT * FROM penggunaan join pelanggan using(id_pelanggan) ");
	while($penggunaan=mysqli_fetch_array($querypenggunaan))
	{
	?>
	
<tr><td><?php echo $penggunaan['id_penggunaan'];?></td>
	<td><?php echo $penggunaan['nama_pelanggan'];?></td>
	<td><?php echo $penggunaan['bulan'];?></td>
	<td><?php echo $penggunaan['tahun'];?></td>
	<td><?php echo $penggunaan['meter_awal'];?></td>
	<td><?php echo $penggunaan['meter_akhir'];?></td>
	<td> <a href="hps_datapenggunaan.php?id_penggunaan=<?=$penggunaan['id_penggunaan'];?>"><button type="button" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button></a>
		 
</td>		
	
	<?php
	}
	?>
										</tr>
               
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
				</div>
        </div>
               
    </div>
	
	
		
		
		
               
   
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
		 </div>
     <!-- /. WRAPPER  -->
	 </div>
	 
	 <footer>
	<table align="center">
	<tr>
	<td><br/>©2019 || Paysel</td>
	</table>
	</tr>
	</footer>
	 
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="../assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="../assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="../assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>

         <!-- CUSTOM SCRIPTS -->
    <script src="../assets/js/custom.js"></script>
	
	
	
</body>
</html>
