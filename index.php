<?php
include"header.php";
?>

  <body style="background:#F3F3F3;">

  <nav class="navbar navbar-expand-lg navbar-light">
 <a class="navbar-brand" href="index.php"><i class="fa fa-flash">PAYSEL</i></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
       </ul>
    <form class="form-inline my-2 my-lg-0"><ul class="navbar-nav mr-auto">
      
     <li class="nav-item">
        <a class="nav-link" href="index.php"> Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#daftar">Daftar</a>
      </li>  
         
   </ul>
       </form>
  </div>
</nav>
  <div class="container">
  <div id="info">
  <h4>PPOB</h4>
  <h6>
    PPOB (Payment Point Online Bank) adalah sebuah sistem pembayaran online dengan memanfaatkan fasilitas perbankan. Kehadiran PPOB memberikan kemudahan bagi pelanggan dalam melakukan pembayaran dimana saja dan kapanpun. Bisnis PPOB atau bisnis loket pembayaran dapat dijadikan sebagai usaha sampingan terutama bagi pemilik warnet, wartel, warung, toko dll.</h6>
  </div>
  <br/><div class="carousel">

      <img class="d-block w-100" src="gambar/2.jpg">
    </div>
<br/>
<div id="daftar">
<br/>
  <h4>Pendaftaran Akun Pelanggan Paysel</h4>

<br/>
                                    <form role="form" action="prosesdaftar.php" method="POST">
                      <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                        <label> Username</label>
                        <input class="form-control" type="text" name="username" id="username" placeholder="Masukan Username" autocomplete="off" required >
                       
						
						
                        </div>
                      </div>
                
                       <div class="col-md-6">
                        <div class="form-group">
                         <label> Password</label>
                        <input class="form-control" type="password" name="password" id="password" placeholder=" Masukan Password" autocomplete="off" required />
                         
                        </div>
                        </div>
                        <!--//untuk tutup div class row-->
                        </div>
                        <div class="row">
                        <div class="col-lg-6">
                        <div class="form-group">
						 <label> Nomor KWH</label>
                        <input class="form-control" type="number" name="nomor_kwh" id="nomor_kwh" placeholder="Masukan Nomor KWH" autocomplete="off"required />
                          
                         
                        </div>
                        </div>
                        <div class="col-lg-6">
                        <div class="form-group">
                          <label >Nama Lengkap</label>
                        <input class="form-control" type="text" name="nama_pelanggan" id="nama_pelanggan" placeholder="Masukan Nama Lengkap" autocomplete="off" required />
                        
                        </div>
                        </div>
                        <!--untuk tutup div class row-->
                        </div>
                        
                        <div class="row">
                        <div class="col-lg-6">
                        <div class="form-group">
						 <label> Alamat</label>
                        <input class="form-control" type="text" name="alamat" id="alamat" placeholder="Masukan Alamat anda" autocomplete="off" required />
                         
                        
                        </div>
                        </div>
						
						<div class="col-lg-6">
                        <div class="form-group">
						 <label>Id Tarif</label>
                        <select class="form-control" name="id_tarif">
						<?php
						include "koneksi.php";
						$query_tarif=mysqli_query($konek,"SELECT * FROM tarif");
						while($tarif=mysqli_fetch_array($query_tarif)){?>
						<option value="<?php echo $tarif['id_tarif'];?>"><?php echo $tarif['daya'] ,"watt-",$tarif['tarifperkwh'],"perkwh";?></option>
						<?php
						}
						?>
                        </select>
                        </div>
                        </div>
						<!--tutup row-->
						</div>
					<div class="row" style="float:right;">
                    <div class="col-lg-6">  
                    
                    <button class="btn btn-primary" type="submit" name="simpan">Daftar</button>
                  </div>
                </div>
				</div>
				<br/>
				<br/>
				
				<div class="row" style="float:right;">
                 <div class="col-lg-12">
                Bila Anda sudah Daftar silahkan<a href="login/index.php"><font color="blue"> Login</font></a>
                      </div></div>
					  </div>
			  
      
                        
                

              
          
                                        </div>
            
</div>
</div>
</form>
</div>



  <?php
  include "footer.php";
  ?>