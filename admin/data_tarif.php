<?php
include"header.php";
?>
			<body>
    <div id="wrapper">
<?php 
include"navbar.php";
?>
<div id="page-wrapper" >
		
            <div id="page-inner">
		
                    <br/>
			<br/>
		
		
		
		
		    <!-- /. ROW  -->
            <div class="row">
                <div class="col-md-4">
                  <!--  Tambah Data Tarif Listrik -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Tambah Data Tarif Listrik
                        </div>
                        <div class="panel-body">
                           <div class="col-md-12">
                                    <form role="form" action="ptmbhdata_tarif.php" method="POST">
                                            
											<div class="row">
													<div class="col-lg-12">
												<div class="form-group">
													<label> Daya</label>
												<input class="form-control" type="number" name="daya" placeholder="Input Daya Listrik" autocomplete="off" required />
												</div>
												</div>
												<div class="col-lg-12">
												<div class="form-group">

                                                                
													<label> Tarif Perkwh</label>
													<div class="form-group input-group">
                                            <span class="input-group-addon">Rp.</span>
                                            <input class="form-control" type="number" name="tarifperkwh" placeholder="Input Tarif Perkwh" autocomplete="off" required />
												</div>
												</div>
												</div>
												
											<br/>
												<div class="col-lg-12">
												<div class="form-group">
										<button style="float:left;" class="btn btn-primary" name="tambah">Simpan</button>
										</div>
										</div>
								
								</div>
       
                            </div>
                        </div>
                    </div>
                     <!-- End  Tambah Data Tarif Listrik -->
                </div>
                <div class="col-md-8">
                     <!-- Data Tarif Listrik  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Data Tarif Listrik
                        </div>
                        <div class="panel-body">
						 <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
					  
 					<th>No Tarif</th>
					                  <th>Daya</th>
                  <th>Tarif perkwh</th>                  
                  
                  <th>Aksi</th>
           
	
	</tr> </thead>
	<tbody>
              <?php
include"koneksi.php";
  $query_tarif=mysqli_query($konek,"SELECT *from tarif");
  while($tarif=mysqli_fetch_array($query_tarif))
  { 
  ?> 
	
<tr><td><?=$tarif['id_tarif'];?></td>
                
                 
                 <td><?=$tarif['daya'];?></td>
                 <td><?=$tarif['tarifperkwh'];?></td>
				 <td><a href="edit_datatarif.php?id_tarif=<?=$tarif['id_tarif'];?>"><button type="button" class="btn btn-info"><i class="fa fa-edit"></i></button></a>
		 <a href="hps_datatarif.php?id_tarif=<?=$tarif['id_tarif'];?>"><button type="button" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button></a>
</td>
	
	
	<?php
	}
	?>  
	</tr>
               
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->

						
						
						</div>
						
                                               <!-- End  data tarif lisrik  -->
                </div>
            </div>
                <!-- /. ROW  -->
            
		
		
		
		
		
               
   
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
		 </div>
     <!-- /. WRAPPER  -->
	 </div>
	 
	 <footer>
	<table align="center">
	<tr>
	<td><br/>©2019 || Paysel</td>
	</table>
	</tr>
	</footer>
	 
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="../assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="../assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="../assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>

         <!-- CUSTOM SCRIPTS -->
    <script src="../assets/js/custom.js"></script>
	
	
	
</body>
</html>
