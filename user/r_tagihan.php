<?php
include "koneksi.php";
session_start();
if(!isset($_SESSION['id_pelanggan']))
{ 
  header("location:../login/index.php");
}
else{
  $query_pelanggan=mysqli_query($konek,"SELECT * FROM pelanggan where id_pelanggan='$_SESSION[id_pelanggan]'");
  $pelanggan=mysqli_fetch_array($query_pelanggan);


}
?>

<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>PPOB PAYSEL</title>
  <!-- BOOTSTRAP STYLES-->
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="../assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   <!-- TABLE STYLES-->
    <link href="../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />


</head>
<body>

    <div id="wrapper">
        
        <?php
        include"navbar.php";
        ?>

<div id="page-wrapper" >
  
            <div id="page-inner">
      
              

        <br/>
       
              <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <h5><b>Riwayat Tagihan</b></h5>
                        
                        </div>
 
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
            
  <th>No Tagihan</th>
  <th>Bulan Bayar</th> 
  <th>Tahun</th>
  <th>Jumlah Meter</th>
  <th>Status</th>
  
  
  </tr> </thead>
  <tbody>
              <?php
              include"koneksi.php";
     $query_tagihan=mysqli_query($konek,"SELECT * FROM tagihan WHERE id_pelanggan='$_SESSION[id_pelanggan]'");
  while($tagihan=mysqli_fetch_array($query_tagihan))
  {
    ?>

<tr><td><?php echo $tagihan['id_tagihan'];?></td>
  <td><?php echo $tagihan['bulan'];?></td>
  <td><?php echo $tagihan['tahun'];?></td>
  <td><?php echo $tagihan['jumlah_meter'];?></td>
  <td><?php echo $tagihan['status'];?></td>
  
  
    
  <?php
}
?>
  
  
  
  </tr>
               
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
</div>
        </div>
               
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
   
   
    <?php
    include"footer.php";
    ?>
  
</body>
</html>
